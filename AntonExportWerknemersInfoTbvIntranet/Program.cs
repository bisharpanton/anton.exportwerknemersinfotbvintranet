﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;
using Ridder.Client.SDK.Extensions.Logging;
using Ridder.Client.SDK.SDKParameters;
using System.IO;

namespace AntonExportWerknemersInfoTbvIntranet
{
    public class Program
    {
        static void Main(string[] args)
        {
            var _sessionMain = new SdkSession();
            Ridder.Common.LoginCompany[] companies;

            try
            {
                _sessionMain.Login();
                companies = _sessionMain.Sdk.GetAdministrations();
            }
            catch
            {
                string message = "Inloggen in Ridder iQ is mislukt.";
                Console.WriteLine("Inloggen Main company mislukt.");
                SendEmailToJacco(message); //Nog niet ingericht, inloggen in Main Company moet gewoon altijd lukken.
                return;
            }

            Console.WriteLine("Ingelogd in Main company.");
            //string targetDir = @"C:\Temp\Anton";
            string targetDir =
                @"\\srv-dc01\dfs\Anton\Anton Groep\Ridder documenten\Tijdelijke locatie export files Intranet";
            
            Guid reportExportEmployeeInfo = new Guid("6a060abb-be47-41af-90b1-61aec53b99d4");
            List<string> resultFiles = new List<string>();
            List<string> pictureFiles = new List<string>();

            foreach (var company in companies)
            {
                var companyConfiguration = new SdkConfiguration
                {
                    CompanyName = company.CompanyName,
                    UserName = "SDK_ADMIN",
                    PersistSession = false,
                    Password = "R!dder$dk_@dmin"
                };

                var companySession = new SdkSession(companyConfiguration);

                try
                {
                    companySession.Login();
                }
                catch
                {
                    string message = $"Inloggen in bedrijf {company.CompanyName} is mislukt. De export is overgeslagen.";
                    Console.WriteLine(message);
                    LogToRidderiQ(_sessionMain, message);
                    continue;
                }

                Console.WriteLine($"Ingelogd in {company.CompanyName}.");

                bool exportReportExists = DetermineIfExportReportExistsInCompany(companySession, reportExportEmployeeInfo);
                if (!exportReportExists)
                {
                    string message = $"Rapport 'Export werknemersinfo tbv Intranet' bestaat niet in bedrijf {company.CompanyName}. De export is overgeslagen.";
                    Console.WriteLine(message);
                    LogToRidderiQ(_sessionMain, message);
                    continue;
                }

                var rsCRM = companySession.GetRecordsetColumns("R_CRMSETTINGS", "PK_R_CRMSETTINGS", "", "");
                rsCRM.MoveFirst();
                int crmId = (int) rsCRM.Fields["PK_R_CRMSETTINGS"].Value;
                
                string targetFilename = $"{targetDir}\\{company.CompanyName}.csv";
                
                var exportResult = companySession.Sdk.ExportReport(crmId, "R_CRMSETTINGS", DesignerScope.User,
                    reportExportEmployeeInfo, Guid.Empty, targetFilename, ExportType.Csv, false);

                if (exportResult.HasError)
                {
                    string message =
                        $"Export 'werknemersinfo tbv Intranet' bij bedrijf {company.CompanyName} is mislukt, oorzaak: \n{exportResult.GetResult()}.";
                    Console.WriteLine(message);
                    LogToRidderiQ(_sessionMain, message);
                    continue;
                }

                resultFiles.Add(targetFilename);

                Console.WriteLine($"Haal de foto's op van bedrijf {company.CompanyName}.");
                List<string> pictureFilesFromCompany = GetPicturesFromCompany(targetDir, companySession);

                if (pictureFilesFromCompany != null)
                {
                    pictureFiles.AddRange(pictureFilesFromCompany);
               }

                Console.WriteLine($"Export voltooid voor bedrijf {company.CompanyName}.");
                companySession.Logout();
            }
            
            //Export resultfiles to FTP
            Console.WriteLine("Start uploaden van gecreëerde export-bestanden naar FTP.");
            
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential("ridderdb@ferryheijnen.nl", "CFo6zM1BmVdF");
                foreach (string file in resultFiles)
                {
                    client.UploadFile("ftp://ridder.ferryheijnen.nl/" + Path.GetFileName(file), WebRequestMethods.Ftp.UploadFile, file);
                }

                foreach (string file in pictureFiles)
                {
                    client.UploadFile("ftp://ridder.ferryheijnen.nl/Fotos/" + Path.GetFileName(file), WebRequestMethods.Ftp.UploadFile, file);
                }
            }

            Console.WriteLine("Alle gecreëerde export-bestanden zijn geüpload naar FTP.");
        }

        private static List<string> GetPicturesFromCompany(string targetDir, SdkSession companySession)
        {
            var rsEmployees = companySession.GetRecordsetColumns("R_EMPLOYEE", "PK_R_EMPLOYEE, FK_PERSON",
                $"(EMPLOYMENTTERMINATIONDATE IS NULL OR EMPLOYMENTTERMINATIONDATE > '{DateTime.Now.Date:yyyyMMdd}')");
            
            if (rsEmployees.RecordCount == 0)
            {
                return null;
            }

            var persons = companySession.GetRecordsetColumns("R_PERSON", "PK_R_PERSON, PICTURE",
                $"PICTURE IS NOT NULL AND PK_R_PERSON IN ({string.Join(",", rsEmployees.DataTable.AsEnumerable().Select(x => x.Field<int>("FK_PERSON")))})").DataTable.AsEnumerable()
                .Select(x => new Person()
                {
                    PersonId = x.Field<int>("PK_R_PERSON"),
                    Picture = x.Field<Byte[]>("PICTURE"),
                });
            
            List<string> result = new List<string>();
            rsEmployees.MoveFirst();

            while (!rsEmployees.EOF)
            {
                int employeeId = (int)rsEmployees.Fields["PK_R_EMPLOYEE"].Value;
                int personId = (int)rsEmployees.Fields["FK_PERSON"].Value;
                string companyName = companySession.Configuration.CompanyName.Replace(" & ", "-").Replace(' ', '-');
                
                string targetFileName = $"{targetDir}\\Fotos\\{companyName}_{employeeId}.jpg";

                if (!persons.Any(x => x.PersonId == personId))
                {
                    rsEmployees.MoveNext();
                    continue;
                }

                var picture = persons.First(x => x.PersonId == personId).Picture;
                File.WriteAllBytes(targetFileName, picture);

                result.Add(targetFileName);

                rsEmployees.MoveNext();
            }


            return result;
        }

        private class Person
        {
            public int PersonId{ get; set; }
            public Byte[] Picture { get; set; }
        }

        private static bool DetermineIfExportReportExistsInCompany(SdkSession companySession, Guid reportExportEmployeeInfo)
        {
            var rsUserReports = companySession.GetRecordsetColumns("R_USERREPORT", "PK_R_USERREPORT",
                $"PK_R_USERREPORT = '{reportExportEmployeeInfo}'");

            return rsUserReports.RecordCount > 0;
        }

        private static void SendEmailToJacco(string message)
        {
            //Nog in te richten
        }

        private static void LogToRidderiQ(SdkSession session, string message)
        {
            var rsLog = session.GetRecordsetColumns("C_LOG", "LOG", "PK_C_LOG = -1");
            rsLog.AddNew();
            rsLog.Fields["LOG"].Value = message;
            rsLog.Update();
        }


        public class CompanyPictures
        {
            public string Company { get; set; }
            public List<string> PictureFiles { get; set; }
        }

    }
}
